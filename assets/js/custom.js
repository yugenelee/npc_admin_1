$(document).ready(function(){
    $(".locker-item-wrapper").click(function(){

        var layout = $(this).attr("layout");
        $(".locker-layout").addClass("hidden");
        $("."+layout).removeClass("hidden");
    });
    $(".item-locker-li").click(function(){
        var layout_class = $(this).attr("layout-class");
        $("."+layout_class).parent().find('.col').addClass("opacity-show");
        $("."+layout_class).removeClass("opacity-show");
    });
});